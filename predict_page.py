import streamlit as st
import xml.dom.minidom

import numpy as np
import pandas as pd
#-------------------------------------------------
from sklearn.utils import shuffle
from keras.preprocessing.text import Tokenizer
from keras.models import load_model
from keras.utils import pad_sequences


def load_model_func():
    filename = 'sentimental_model'
    loaded_model = load_model(filename)
    return loaded_model

model = load_model_func()

def prediction_page_func():
    st.title('Software Prediction Sentimental App')
    st.write("""### App predicts the sentiment on reviews about books""")
    st.write("""###### The sentiment would be either 'Positive' or 'Negative'""")

    review_text = st.text_area(
        label="Sentimental Review", 
        placeholder="Input your text...")
    
    btn_predict = st.button(label="Predict", type='primary')
    
    if btn_predict:
        numWords = 1000
    
        negavative_xml_file = xml.dom.minidom.parse(r"books/negative.review.xml")
        positive_xml_file = xml.dom.minidom.parse(r"books/positive.review.xml")
        #----------------------------------------------------------
        pos_root = positive_xml_file.documentElement
        neg_root = negavative_xml_file.documentElement
        #----------------------------------------------------------
        pos_reviews = pos_root.getElementsByTagName('review')
        neg_reviews = neg_root.getElementsByTagName('review')
        #----------------------------------------------------------
        postive_review_df = xml_to_csv(pos_reviews)
        negative_review_df = xml_to_csv(neg_reviews)
        pos_nag_df = pd.concat([postive_review_df, negative_review_df])
        #----------------------------------------------------------
        shuffled_df = shuffle(pos_nag_df)
        #----------------------------------------------------------
        tokenizer = Tokenizer(num_words=numWords)
        tokenizer.fit_on_texts(shuffled_df['review'])
        #----------------------------------------------------------
        # Mapping the real world text with already generated tokens
        tokens_seq = tokenizer.texts_to_sequences(np.array([review_text]))
        
        #----------------------------------------------------------
        # Preforming padding and truncting of the mapped text
        tokens_pad = pad_sequences(tokens_seq,
                            padding='pre',
                            maxlen=50)
        # Make a prdiction
        y_pred = model.predict(tokens_pad)
        # Format the outcomes to a 0 for negative and a 1 for positive
        
        if y_pred >= 0.5:
            cls_pred = "Positive Sentiment"
        else:
            cls_pred = "Negative Sentiment" 
        st.subheader(f"{cls_pred} of the book")



def xml_to_csv(reviews):
    review = []
    rate = []
    
    for views in reviews:
        review_text = views.getElementsByTagName('review_text')[0].childNodes[0].nodeValue
        rating = views.getElementsByTagName('rating')[0].childNodes[0].nodeValue
    
        review.append(review_text.replace("\n", ""))
        rate.append(rating.replace("\n", ""))
        
    rate = [1 if float(i)>=3 else 0 for i in rate] # 0.0 => Negative and 1.0 => Positive
    
    data = {"review":review, "rating":rate}
    df = pd.DataFrame(data)
    return df

